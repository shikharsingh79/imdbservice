package com.kenstream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.api.java.function.ReduceFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

public class ImdbService {

    public static void main(String[] args) {

        Options options = new Options();

        options.addOption(OptionBuilder.withArgName("ratings")
                .withLongOpt("title_with_ratings")
                .hasArg()
                .withDescription("filepath to file with title and ratigs")
                .isRequired(true)
                .create("t"));

        options.addOption(OptionBuilder.withArgName("title")
                .withLongOpt("title_languges")
                .hasArg()
                .withDescription("Titles in different langugaes")
                .isRequired(true)
                .create("o"));

        options.addOption(OptionBuilder.withArgName("actors")
                .withLongOpt("actor details")
                .hasArg()
                .withDescription("details about actors, name.basics")
                .isRequired(true)
                .create("c"));
        try {

            CommandLine cmd = new GnuParser().parse(options, args);
            run(cmd.getOptionValue("t"), cmd.getOptionValue("o"), cmd.getOptionValue("c"));

        } catch (
                ParseException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }

    }

    private static void run(String t, String o, String c) {
        Schemas schemas = new Schemas();
        SparkSession spark = SparkSession
                .builder()
                .appName("Java Spark SQL basic example")
                .config("spark.master", "local")
                .getOrCreate();
        Dataset<Row> peopleDFCsv = spark.read().format("csv")
                .option("delimiter", "\t")
                .option("header", true)
                .schema(schemas.getMovieSchema())
                .load(t);
        Dataset<Row> movieTitles = spark.read().format("csv")
                .option("delimiter", "\t")
                .option("header", true)
                .schema(schemas.getTitlesSchema())
                .load(o);
        Dataset<Row> characters = spark.read().format("csv")
                .option("delimiter", "\t")
                .option("header", true)
                .schema(schemas.getCharacters())
                .load(c);
        Dataset<Row> filtered = peopleDFCsv.filter("numVotes > 50");
        final Long averageVotes = calculateAverageVotes(filtered) / filtered.count();
        Dataset<Tuple2<String, Float>> moviesByVote = filtered.map(new MapFunction<Row, Tuple2<String, Float>>() {
            public Tuple2<String, Float> call(Row row) {
                float some = ((float) row.getInt(2) * 1000000 / averageVotes);
                Float score = (some / averageVotes) * row.getFloat(1);
                return new Tuple2<String, Float>(row.getString(0), score);
            }
        }, Encoders.tuple(Encoders.STRING(), Encoders.FLOAT()));
        Dataset<Tuple2<String, Float>> ordered =
                moviesByVote.orderBy(org.apache.spark.sql.functions.col("_2").desc()).limit(10);
        Dataset<Row> joined = ordered.join(movieTitles, movieTitles.col("titleId").equalTo(ordered.col("_1")), "inner")
                .select(movieTitles.col("title"),
                        movieTitles.col("isOriginalTitle"));
        Dataset<Row> cha =
                ordered.join(characters, characters.col("knownForTitles").contains(ordered.col("_1")), "inner")
                        .select(characters.col("primaryName"), ordered.col("_1"));
        System.out.println("Top rated movies:");
        ordered.show();
        System.out.println("All the titles of the top movies");
        joined.show();
        System.out.println("The actors associated with top movies");
        cha.show();
    }

    private static Integer calculateAverageVotes(Dataset<Row> rowDataset) {

        Dataset<Integer> dataset = rowDataset.map(new MapFunction<Row, Integer>() {
            public Integer call(Row row) {
                return row.getInt(2);
            }
        }, Encoders.INT());

        return dataset.reduce(new ReduceFunction<Integer>() {
            public Integer call(Integer integer, Integer t1) {
                return integer + t1;
            }
        });
    }

}
