package com.kenstream;

import org.apache.avro.Schema;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import static org.apache.spark.sql.types.DataTypes.StringType;

public class Schemas {

    public StructType getMovieSchema () {
        StructField[] movie = {
                new StructField("tconst", StringType, false, Metadata.empty()),
                new StructField("averageRating", DataTypes.FloatType, false, Metadata.empty()),
                new StructField("numVotes", DataTypes.IntegerType, false, Metadata.empty()),
        };

        return new StructType(movie);
    }

    public StructType getTitlesSchema() {

        StructField[] movieTitles = {
                new StructField("titleId", StringType, false, Metadata.empty()),
                new StructField("ordering", StringType, false, Metadata.empty()),
                new StructField("title", StringType, false, Metadata.empty()),
                new StructField("region", StringType, true, Metadata.empty()),
                new StructField("language", StringType, true, Metadata.empty()),
                new StructField("types", StringType, true, Metadata.empty()),
                new StructField("attributes", StringType, true, Metadata.empty()),
                new StructField("isOriginalTitle", StringType, true, Metadata.empty())
        };
        return new StructType(movieTitles);
    }

    public StructType getWriters() {
        StructField[] writers = {
                new StructField("tconst", StringType, false, Metadata.empty()),
                new StructField("directors", StringType, false, Metadata.empty()),
                new StructField("writers", StringType, false, Metadata.empty())

        };
        return new StructType(writers);
    }

    public StructType getCharacters() {
        StructField[] characters = {
                new StructField("nconst", StringType, false, Metadata.empty()),
                new StructField("primaryName", StringType, false, Metadata.empty()),
                new StructField("birthYear", StringType, false, Metadata.empty()),
                new StructField("deathYear", StringType, true, Metadata.empty()),
                new StructField("primaryProfession", StringType, true, Metadata.empty()),
                new StructField("knownForTitles", StringType, true, Metadata.empty()),
        };

        return new StructType(characters);
    }
}
