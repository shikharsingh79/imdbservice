# ImdbService 

## Installation 

This is a maven project. 

Download the source code and run maven clean package.

A jar file with all dependencies will be created. Take this jar file and run it locally or on a cluster. 

The command to run jar is 

java -jar imdbservice.jar -t "path to title.ratings" -o "path to title.akas" -c "path to name.basics"

The output is simply printed to console and meets the requirements mentioned in the test document. 

No sql statements have been programmed in the application, and the application uses pure spark functionality. 

## Not in place:
Test are not in place. I did not find to complete the tests, sorry. 

